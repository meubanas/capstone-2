/* 

================== Most Important ==================
* Issue 1 :
In uploads folder you need create 3 folder like bellow.
Folder structure will be like: 
public -> uploads -> 1. products 2. customize 3. categories

* Issue 2:
For admin signup just go to the auth 
controller then newUser obj, you will 
find a role field. role:1 for admin signup & 
role: 0 or by default it for customer signup.
go user model and see the role field.


* Deploying in Heroku domains
with different port client and server = to make it one ports
  - add in .env file for your port to be use in HOROKU (sample 3000)
  - git add -A
  - git commit -m ""
  - heroku create ""
  - heroku local - running the codes/server before push in heroku domain website
                   to check if there an error
  - add Procfile - (web: node app.js) name at script/start of your package.json

*/

const express = require("express");
const app = express();
require("dotenv").config();
const mongoose = require("mongoose");
const morgan = require("morgan");
const cookieParser = require("cookie-parser");
const cors = require("cors");

// Import Router
const authRouter = require("./routes/auth");
const categoryRouter = require("./routes/categories");
const productRouter = require("./routes/products");
const brainTreeRouter = require("./routes/braintree");
const orderRouter = require("./routes/orders");
const usersRouter = require("./routes/users");
const customizeRouter = require("./routes/customize");

// Import Auth middleware for check user login or not~
const { loginCheck } = require("./middleware/auth");

// Database Connection

  mongoose.connect("mongodb+srv://admin:admin131@zuitt.cfuxf.mongodb.net/Collections?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  }
);

let db = mongoose.connection

  db.on('error', () => console.error.bind(console, 'Connection Error'))
  db.once('open', () => console.log('Now connected to MongoDB Atlas.'))


// Middleware
app.use(morgan("dev"));
app.use(cookieParser());
app.use(cors());

// Heroku-to make one server copy the build folder from client folder and paste it on public folder(server)
app.use(express.static("public"));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// Routes
app.use("/api", authRouter);
app.use("/api/user", usersRouter);
app.use("/api/category", categoryRouter);
app.use("/api/product", productRouter);
app.use("/api", brainTreeRouter);
app.use("/api/order", orderRouter);
app.use("/api/customize", customizeRouter);

// if(process.env.NODE_ENV === 'productions') {
//   app.use(express.static('public'));
// }

// rEACT 11/10/2021
const path = require('path');
if (process.env.NODE_ENV === 'production') {
  // Serve any static files
  app.use(express.static(path.join(__dirname, 'public')));
// Handle React routing, return all requests to React app
  app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
  });
}


// Run Server
// const host = '0.0.0.0';
const PORT = process.env.port || 3000;
// // app.listen(port, () => {
// //   console.log(`Server is running on, ${port}`);
// // });

app.listen(process.env.PORT || 3000, function(){
  console.log("Express server listening on port %d in %s mode", this.address().port, app.settings.env);
});
